// colors
import 'package:flutter/material.dart';

final Color primaryColor = Color(0xFFfbc02d);
final Color secondaryColor = Color(0xFFfbc02d);

final Color appBarLightColor = Color(0xFFFFFFFF);
final Color appBarDarkColor = Color(0xFF333333);
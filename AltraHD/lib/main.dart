import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:videostatus/screens/HomeScreen.dart';
//import 'package:videostatus/screens/SplashScreen.dart';

import 'config/colors.dart';

void main() => runApp(App());

class App extends StatelessWidget {

  final FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: "/home",
      routes: {
        //"/": (context) => SplashScreen(),
        "/": (context) => HomeScreen()
      },
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      theme: ThemeData(
        primaryColor: primaryColor,
        primaryColorBrightness: Brightness.dark,
        accentColor: secondaryColor,
        accentColorBrightness: Brightness.light,
        fontFamily: "poppins",
        buttonTheme: ButtonThemeData(
          buttonColor: secondaryColor,
          splashColor: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
        ),
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: appBarLightColor,
          brightness: Brightness.light,
          elevation: 0,
          textTheme: TextTheme(
            headline6: TextStyle(color: Colors.black),
          ),
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          actionsIconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: primaryColor,
        primaryColorBrightness: Brightness.dark,
        accentColor: secondaryColor,
        accentColorBrightness: Brightness.light,
        fontFamily: "poppins",
        buttonTheme: ButtonThemeData(
          buttonColor: secondaryColor,
          splashColor: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
        ),
        appBarTheme: AppBarTheme(
          color: appBarDarkColor,
          brightness: Brightness.dark,
          elevation: 0,
          textTheme: TextTheme(
            headline6: TextStyle(color: Colors.white),
          ),
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          actionsIconTheme: IconThemeData(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

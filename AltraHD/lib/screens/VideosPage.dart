import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:videostatus/config/colors.dart';
import 'package:videostatus/screens/VideoPlayerScreen.dart';

class VideosPage extends StatefulWidget {
  @override
  _VideosPageState createState() => _VideosPageState();
}

class _VideosPageState extends State<VideosPage> {

  ScrollController _scrollController;

  final Firestore _db = Firestore.instance;

  final int limit = 10;
  List<DocumentSnapshot> videos;
  DocumentSnapshot lastDocument;

  bool loading = true;

  @override
  void initState() {
    fetchVideos();
    _scrollController = ScrollController()
    ..addListener(() {
        if(_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
            print("loading more uploads");
          fetchMoreVideos();
        }
    });
    super.initState();
  }

  fetchVideos() async {
    try {
      setState(() {
        loading = true;
      });

      QuerySnapshot querySnapshot = await _db
          .collection("videos")
          .orderBy("date", descending: true)
          .limit(limit)
          .getDocuments();

      lastDocument = querySnapshot.documents.last;

      setState(() {
        videos = querySnapshot.documents;
        loading = false;
      });
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  fetchMoreVideos() async {
    try {
      

      QuerySnapshot querySnapshot = await _db
          .collection("videos")
          .startAfterDocument(lastDocument)
          .orderBy("date", descending: true)
          .limit(limit)
          .getDocuments();

      lastDocument = querySnapshot.documents.last;

      setState(() {
        videos.addAll( querySnapshot.documents);
        loading = false;
      });
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return Center(
        child: Container(
            child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        )),
      );
    } else {
      if (videos != null) {
        return Container(
          child: RefreshIndicator(
            onRefresh: () async {
              fetchVideos();
              return null;
            },
            child: ListView.builder(
              controller: _scrollController,
              itemBuilder: (ctx, i) {
                return Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 10,
                      ),
                      child: Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            blurRadius: 20,
                            color: Color(0x55000000),
                            offset: Offset(0, 10),
                          )
                        ]),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(16),
                          child: AspectRatio(
                            aspectRatio: 16 / 9,
                            child: CachedNetworkImage(
                              imageUrl: videos[i].data["thumbnail"],
                              placeholder: (ctx, str) => Image.asset(
                                "assets/loading_new.gif",
                                fit: BoxFit.cover,
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned.fill(
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: primaryColor,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => VideoPlayerScreen(
                                  video: videos[i],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                );
              },
              itemCount: videos.length,
            ),
          ),
        );
      } else {
        return Center(
          child: Container(
            child: Image(
              image: AssetImage("assets/no_videos.png"),
              width: 300,
              height: 300,
            ),
          ),
        );
      }
    }
  }
}

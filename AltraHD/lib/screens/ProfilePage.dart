import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:videostatus/config/ads.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final FirebaseAuth auth = FirebaseAuth.instance;

  FirebaseUser user;

  @override
  void initState() {
    super.initState();
    getUser();
    showAd();
  }

  getUser() async {
    user = await auth.currentUser();
    setState(() {});
  }

  showAd() {
    InterstitialAd myInterstitial = InterstitialAd(
      adUnitId: Platform.isAndroid?admobAndroidInterstitialID:admobIOSInterstitialID,
    );

    myInterstitial
      ..load()
      ..show(
        anchorType: AnchorType.bottom,
        anchorOffset: 0.0,
        horizontalCenterOffset: 0.0,
      );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      width: width,
      child: SingleChildScrollView(
        child: user != null
            ? Column(
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(150),
                    child: CachedNetworkImage(
                      imageUrl: user.photoUrl,
                      placeholder: (ctx, s) => Image.asset(
                        "assets/avatar.png",
                        width: 200,
                        height: 200,
                        fit: BoxFit.cover,
                      ),
                      width: 200,
                      height: 200,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  RaisedButton(
                    onPressed: () {
                      auth.signOut().then((v) {
                        Navigator.pushReplacementNamed(context, "/");
                      });
                    },
                    child: Text("Logout"),
                  )
                ],
              )
            : Text("Loading"),
      ),
    );
  }
}

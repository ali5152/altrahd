import 'dart:async';
import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinning_wheel/flutter_spinning_wheel.dart';
import 'package:videostatus/config/ads.dart';

class GamePage extends StatefulWidget {
  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {

  final StreamController _dividerController = StreamController<int>();

  @override
  void initState() {
    showAd();
    super.initState();
  }

  @override
  void dispose() {
    _dividerController.close();
    super.dispose();
    
  }
  

  showAd() {
    InterstitialAd myInterstitial = InterstitialAd(
      adUnitId: Platform.isAndroid?admobAndroidInterstitialID:admobIOSInterstitialID,
    );

    myInterstitial
      ..load()
      ..show(
        anchorType: AnchorType.bottom,
        anchorOffset: 0.0,
        horizontalCenterOffset: 0.0,
      );
  }

  
  @override
  Widget build(BuildContext context) {

    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Spin This Wheel"),
            SizedBox(height: 20,),
            SpinningWheel(
              Image.asset('assets/wheel.png'),
              width: 310,
              height: 310,
              initialSpinAngle:0,
              spinResistance: 0.2,
              canInteractWhileSpinning: false,
              dividers: 14,
              onUpdate: _dividerController.add,
              onEnd: _dividerController.add,
              secondaryImage:
                  Image.asset('assets/wheel_handle.png'),
              secondaryImageHeight: 110,
              secondaryImageWidth: 110,
            ),
            SizedBox(height: 30),
            StreamBuilder(
              stream: _dividerController.stream,
              builder: (context, snapshot) =>
                  snapshot.hasData ? RouletteScore(snapshot.data) : Container(),
            )
          ],
        ),
      );
  }
}
class RouletteScore extends StatelessWidget {
  final int selected;

  final Map<int, String> labels = {
    1: '1',
    2: '0',
    3: '2',
    4: '0',
    5: '3',
    6: '0',
    7: '4',
    8: '0',
    9: '5',
    10: '0',
    11: '6',
    12: '0',
    13: '7',
    14: '0'
  };

  RouletteScore(this.selected);

  @override
  Widget build(BuildContext context) {
    return Text('${labels[selected]} Points',
        style: TextStyle(fontStyle: FontStyle.italic, fontSize: 24.0));
  }
}
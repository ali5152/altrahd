import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:videostatus/config/ads.dart';
import 'package:videostatus/config/texts.dart';
import 'package:videostatus/screens/CategoriesPage.dart';
import 'package:videostatus/screens/GamePage.dart';
import 'package:videostatus/screens/ProfilePage.dart';
import 'package:videostatus/screens/VideosPage.dart';

import 'package:firebase_in_app_messaging/firebase_in_app_messaging.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  // pages
  List pages = [VideosPage(), CategoriesPage(), GamePage(), ProfilePage()];

  @override
  void initState() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );

    if (Platform.isIOS) {
      askNotificationPermission();
    }

    if(Platform.isIOS) {
      FirebaseAdMob.instance.initialize(appId: admobAndroidAppID);
    } else if (Platform.isAndroid) {
      FirebaseAdMob.instance.initialize(appId: admobIOSAppID);
    }

    super.initState();
  }

  void askNotificationPermission() async {
    await _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appName),
        centerTitle: true,
      ),
      body: pages[_currentIndex],
      
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          setState(() => _currentIndex = index);
        },
        items: [
          BottomNavigationBarItem(
            title: Text('Videos'),
            icon: Icon(Icons.ondemand_video),
          ),
          BottomNavigationBarItem(
            title: Text('Categories'),
            icon: Icon(Icons.category),
          ),
          // BottomNavigationBarItem(
          //   title: Text('Game'),
          //   icon: Icon(Icons.gamepad),
          // ),
          // BottomNavigationBarItem(
          //   title: Text('My Profile'),
          //   icon: Icon(Icons.person_outline),
          // ),
        ],
      ),
    );
  }
}

import 'dart:io';
import 'dart:typed_data';

import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:chewie/chewie.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:video_player/video_player.dart';
import 'package:videostatus/config/colors.dart';
import 'package:videostatus/config/texts.dart';

class VideoPlayerScreen extends StatefulWidget {
  final DocumentSnapshot video;

  VideoPlayerScreen({@required this.video});

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;

  @override
  void initState() {
    initVideoPlayer();
    super.initState();
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    chewieController.dispose();
    super.dispose();
  }

  initVideoPlayer() {
    videoPlayerController =
        VideoPlayerController.network(widget.video.data["url"]);

    videoPlayerController.initialize().then((v) {
      setState(() {
        chewieController = ChewieController(
          videoPlayerController: videoPlayerController,
          aspectRatio: videoPlayerController.value.aspectRatio,
          autoPlay: true,
          looping: true,
        );
      });
    });
  }

  Future<void> _shareVideo() async {
    var request =
        await HttpClient().getUrl(Uri.parse(widget.video.data["url"]));
    var response = await request.close();
    Uint8List bytes = await consolidateHttpClientResponseBytes(response);
    await Share.file(appName, 'video.mp4', bytes, 'video/mp4');
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.video.data["title"]),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            chewieController != null
                ? Chewie(
                    controller: chewieController,
                  )
                : AspectRatio(
                    aspectRatio: 16 / 9,
                    child: Image.asset("assets/loading_new.gif"),
                  ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.video.data["title"],
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: ArgonButton(
                  height: 50,
                  width: width,
                  borderRadius: 5.0,
                  color: Color(0xFF7866FE),
                  child: Text(
                    "Share",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                  loader: Container(
                    padding: EdgeInsets.all(10),
                    child: SpinKitRotatingCircle(
                      color: Colors.white,
                    ),
                  ),
                  onTap: (startLoading, stopLoading, btnState) async {
                    if(btnState == ButtonState.Idle) {
                      startLoading();
                      await _shareVideo();
                      stopLoading();
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

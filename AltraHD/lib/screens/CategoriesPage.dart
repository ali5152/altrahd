import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:videostatus/config/colors.dart';
import 'package:videostatus/screens/CategoryVideosPage.dart';

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  bool loading = true;

  List<DocumentSnapshot> categories;

  final Firestore db = Firestore.instance;

  @override
  void initState() {
    super.initState();
    fetchCategories();
  }

  fetchCategories() async {
    try {
      QuerySnapshot querySnapshot =
          await db.collection("categories").getDocuments();
      setState(() {
        categories = querySnapshot.documents;
        loading = false;
      });
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return Center(
        child: Container(
            child: SpinKitChasingDots(
          color: primaryColor,
          size: 50,
        )),
      );
    } else {
      if (categories != null) {
        return Container(
          child: GridView.count(
            crossAxisCount: 2,
            children: categories.map((c) {
              return InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CategoryVideosPage(
                        category: c.documentID,
                      ),
                    ),
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.circular(4),
                  ),
                  margin: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 10,
                  ),
                  padding: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 10,
                  ),
                  child: Text(
                    c.data["name"],
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w900,
                    ),
                    softWrap: true,
                  ),
                ),
              );
            }).toList(),
          ),
        );
      } else {
        return Center(
          child: Container(
            child: Image(
              image: AssetImage("assets/no_videos.png"),
              width: 300,
              height: 300,
            ),
          ),
        );
      }
    }
  }
}

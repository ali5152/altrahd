// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


function signOut() {
    firebase.auth().signOut();
    window.open("index.html", "_SELF")
}
